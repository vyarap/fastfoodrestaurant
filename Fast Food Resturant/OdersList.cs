﻿using Fast_Food_Resturant.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fast_Food_Resturant
{
    public partial class OdersList : UserControl
    {
        private List<TakeAwayModel> orders;
        private readonly DbClass DbClass;
        private TakeAwayModel order;
        public OdersList()
        {
            InitializeComponent();
            this.DbClass = new DbClass();
            LoadOrders();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        public void LoadOrders()
        {
            this.orders = this.DbClass.GetOrdersFromTakeAwayTable();
            this.OrdersList.DataSource = this.orders;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            order = new TakeAwayModel();
            if (OrdersList.SelectedRows.Count != 0)
            {
                DataGridViewRow row = this.OrdersList.SelectedRows[0];
                order.Id = (int)row.Cells["Id"].Value;
                order.FirstName = (row.Cells["FirstName"].Value).ToString();
                order.LastName = (row.Cells["LastName"].Value).ToString();
                order.Email = (row.Cells["Email"].Value).ToString();
                order.Number = (row.Cells["Number"].Value).ToString();
                order.Adress = (row.Cells["Adress"].Value).ToString();
                order.Order = (row.Cells["Order"].Value).ToString();
                DbClass.UpdateOrderInfo(order);
            }       
        }

        private void button1_Click(object sender, EventArgs e)
        {
            order = new TakeAwayModel();
            DataGridViewRow row = this.OrdersList.SelectedRows[0];
            order.Id = (int)row.Cells["Id"].Value;
            DbClass.DeleteOrderById(order.Id);
            LoadOrders();
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            LoadOrders();
        }
    }
}
