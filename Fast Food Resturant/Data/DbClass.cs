﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fast_Food_Resturant.Data
{
    public class DbClass
    {
        //Connecting to DB
        static string connectonString = "Server=localhost; Port=3306; Database=fastfoodresturant; Uid=root; Pwd=root";
        MySqlConnection connection = new MySqlConnection(connectonString);

        //Insering into take_away table
        public bool InsertIntoTakeAwayTable(TakeAwayModel takeAway)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "INSERT INTO fastfoodresturant.take_away(FirstName, LastName, Number, Email, Adress, take_away.Order)  VALUES (@FirstName , @LastName, @Number, @Email ,@Adress, @Order)";
                MySqlCommand command = new MySqlCommand(sql, connection);

                command.Parameters.AddWithValue("@Id", takeAway.Id);
                command.Parameters.AddWithValue("@FirstName", takeAway.FirstName);
                command.Parameters.AddWithValue("@LastName", takeAway.LastName);
                command.Parameters.AddWithValue("@Number", takeAway.Number);
                command.Parameters.AddWithValue("@Email", takeAway.Email);
                command.Parameters.AddWithValue("@Adress", takeAway.Adress);
                command.Parameters.AddWithValue("@Order", takeAway.Order);

                connection.Open();
                IsSuccess = command.ExecuteNonQuery() > 0;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }
            return IsSuccess;
        }

        //Updating order
        public bool UpdateOrderInfo(TakeAwayModel takeAway)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);

            try
            {
                string sql = "UPDATE take_away SET FirstName = @FirstName, LastName = @LastName , Number = @Number , Email = @Email , Adress = @Adress , take_away.Order = @Order  WHERE Id = @Id";
                MySqlCommand command = new MySqlCommand(sql, connection);
                connection.Open();
                command.Parameters.AddWithValue("@Id", takeAway.Id);
                command.Parameters.AddWithValue("@FirstName", takeAway.FirstName);
                command.Parameters.AddWithValue("@LastName", takeAway.LastName);
                command.Parameters.AddWithValue("@Number", takeAway.Number);
                command.Parameters.AddWithValue("@Email", takeAway.Email);
                command.Parameters.AddWithValue("@Adress", takeAway.Adress);
                command.Parameters.AddWithValue("@Order", takeAway.Order);
                IsSuccess = command.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;
        }

        //Deleting order
        public bool DeleteOrderById(int OrderId)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);
            try
            {
                MySqlCommand commandForDeletingDetail = new MySqlCommand("delete from take_away where Id = " + OrderId + " ", connection);
                connection.Open();
                IsSuccess = commandForDeletingDetail.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;

        }

        public List<TakeAwayModel> GetOrdersFromTakeAwayTable()
        {
            List<TakeAwayModel> contacts = new List<TakeAwayModel>();
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT * FROM take_away";
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                TakeAwayModel contact = null;

                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        contact = new TakeAwayModel();
                        contact.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        contact.FirstName = reader.GetString(reader.GetOrdinal("FirstName"));
                        contact.LastName = reader.GetString(reader.GetOrdinal("LastName"));
                        contact.Number = reader.GetString(reader.GetOrdinal("Number"));
                        contact.Email = reader.GetString(reader.GetOrdinal("Email"));
                        contact.Adress = reader.GetString(reader.GetOrdinal("Adress"));
                        contact.Order = reader.GetString(reader.GetOrdinal("Order"));
                        contacts.Add(contact);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return contacts;
        }
    }
}
