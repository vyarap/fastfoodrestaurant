﻿namespace Fast_Food_Resturant.Data
{
    public class TakeAwayModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Number { get; set; }

        public string Email { get; set; }

        public string Adress { get; set; }

        public string Order { get; set; }
    }
}
