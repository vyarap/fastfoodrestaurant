﻿using Fast_Food_Resturant.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fast_Food_Resturant
{
    public partial class TakeAway : UserControl
    {
        public TakeAway()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DbClass dbClass = new DbClass();
            TakeAwayModel takeAway = new TakeAwayModel();
            takeAway.FirstName = FirstNameTextBox.Text;
            takeAway.LastName = LastNameTextBox.Text;
            takeAway.Number = NumberTextBox.Text;
            takeAway.Email = EmailTextBox.Text;
            takeAway.Adress = AdressTextBox.Text;
            takeAway.Order = OrderTextBox.Text;
            bool IsSuccess = dbClass.InsertIntoTakeAwayTable(takeAway);
            if(IsSuccess)
            {
                MessageBox.Show("Added");
            }else
            {
                MessageBox.Show("Error");
            }
        }
    }
}
